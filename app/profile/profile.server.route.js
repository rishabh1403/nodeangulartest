var profileCtrl = require('./profile.server.controller');
var authCtrl = require('../auth/auth.server.controller');
module.exports = function (app) {

    app.post('/api/profile/get', authCtrl.isAuthenticated, profileCtrl.profile);
    app.post('/api/profile/update', authCtrl.isAuthenticated, profileCtrl.updateProfile);



};