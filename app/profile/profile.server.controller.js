var mongoose = require('mongoose');
var key = require('../../serverService/jwt.server.service');
var uuid = require('node-uuid');
var Profile = require('../auth/auth.server.model');

module.exports.profile = function (req, res) {
    Profile.findOne({
        email: req.authEmail
    }, function (err, profile) {
        if (err) {
            res.sendStatus(500);
        } else {
            res.status(200).send(profile);
        }
    });
};
module.exports.updateProfile = function (req, res) {
    console.log(req.body);
    var profile = req.body.profile;
    Profile.findOneAndUpdate({
            email: req.authEmail
        }, profile,
        function (err, profile) {
            if (err) {
                console.log("err in saving");
                res.sendStatus(500);
            } else {
                console.log("err in no saving");
                res.sendStatus(200);

            }


        });


};
