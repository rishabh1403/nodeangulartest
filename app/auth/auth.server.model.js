var mongoose = require('mongoose');

module.exports = mongoose.model('User', {
    email: {
        type: String,
        unique: true
    },
    password: String,
    name: String,
    usertype : String,
    addLineOne : String,
    addLineTwo : String,
    profileImg : String,
    pincode : String,
    status : String
});