var authCtrl = require('./auth.server.controller');
module.exports = function (app) {
    app.post('/api/user/register', authCtrl.register);
    app.post('/api/user/login', authCtrl.login);
};