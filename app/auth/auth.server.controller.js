var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var key = require('../../serverService/jwt.server.service');
var User = require('./auth.server.model');
module.exports.register = function (req, res) {
    console.log(req.body.email + " " + req.body.password);
    var hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
    //console.log(hash);
    var user = new User({
        email: req.body.email,
        password: hash,
        name: req.body.name,
        usertype:req.body.usertype,
        addLineOne:"",
        addLineTwo:"",
        profileImg:"placeholder.jpg",
        pincode:"800001",
        status : "false"
    });
    user.save(function (err) {
        if (err) {
            var error = "Some unknown err";
            if (err.code === 11000) {
                error = "that email is already taken";
                res.json({
                    error: error
                });
            } else {
                res.sendStatus(500);
            }
        } else {
            /// login to send email
            /// will create a unique id using node-uuid
            // will send a link domain.com/verify/generatedID
            // will store the id in database with related email
            // if the url is hit , will grab id and in databse update status of user
            var token = key.getKey(req.body.email);
            res.status(200).send(token);
        }

    });


};
module.exports.login = function (req, res) {
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (!user) {
            res.json({
                error: "invalid email or password"
            });
        } else {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                var token = key.getKey(req.body.email);
                console.log(token);
                res.status(200).send(token);
            } else {
                res.json({
                    error: "invalid email or password"
                });
            }
        }

    });
};

module.exports.isAuthenticated = function (req, res, next) {
    var token = req.query.token || req.body.token;
    key.verifyKey(token, function (result) {
        if (result.success) {
            req.authEmail = result.message.email;
            next();
        } else {
            res.sendStatus(403);
        }

    });
};