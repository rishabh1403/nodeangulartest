var jwt = require('jsonwebtoken');
var jwtKey = require('../serverConfig/jwt.server.config');
module.exports.getKey = function (email) {
    /*    res.json({
            yes: "hitting here"
        });*/
    var user = {
        email: email
    }
    var token = jwt.sign(user, jwtKey.jwtKeySecret, {
        expiresIn: 30 * 24 * 60 * 60 // expires in 30 days
    });
    return token;
    // return the information including token as JSON
    /*res.json({
    success: true,
    message: 'Enjoy your token!',
    token: token
});*/
};
module.exports.verifyKey = function (token, callback) {
    /*
        res.json({
            yes: req.query
        });*/
    //    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    /* res.json({
     token: token
 });*/
    // decode token
    var result = {};
    /*result.success = false;
result.message = "Failed to authenticate token";
return result;*/

    //console.log("token coming here", token);
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, jwtKey.jwtKeySecret, function (err, decoded) {
            //console.log(err);
            if (err) {
                result.success = false;
                result.message = "Failed to authenticate token";
                callback(result);
            } else {
                // if everything is good, save to request for use in other routes
                /*req.decoded = decoded;
next();*/
                // console.log(decoded);
                result.success = true;
                result.message = decoded;
                callback(result);
            }
        });

    } else {

        // if there is no token
        // return an error
        result.success = false;
        result.message = " no token provided";
        callback(result);
        /* res.status(403).send({
     success: false,
     message: 'No token provided.'
 });*/

    }
};