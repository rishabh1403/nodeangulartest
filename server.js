'use strict';
//here we are including our external dependencies

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

//here we are requiring our internal dependencies

var db = require('./serverConfig/db.server.config');

//connecting to db

mongoose.connect(db.url);

// CONNECTION EVENTS
// When successfully connected

mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + db.url);
});

// If the connection throws an error

mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected

mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

//configuring middleware

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

//requiring routes
require('./app/auth/auth.server.route.js')(app);
require('./app/profile/profile.server.route.js')(app);

app.listen(4000, function (err) {
    console.log("server running at http://localhost:4000");
});