(function () {
    angular.module('myApp').config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {


        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: '/app/home/home.html',
                controller: 'appCtrl'
            }).state('profile', {
                url: '/profile',
                templateUrl: '/app/profile/profile.html',
                controller: 'profileCtrl'
            })



        $urlRouterProvider.otherwise('/home');
}]);
}());