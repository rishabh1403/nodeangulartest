(function () {
    angular.module('myApp').controller('appCtrl', ['$scope', '$http', 'sessionFactory','$state',
     function ($scope, $http, sessionFactory,$state) {
        $scope.isLogin = false;
        $scope.errormsg = "";
        $scope.changeLogin = function(){
            $scope.isLogin = !$scope.isLogin;
        }
        $scope.register = function (user) {
            console.log(user.usertype);
            sessionFactory.register(user).then(function (response) {
                if (response.data.error) {
                    $scope.errormsg = response.data.error;
                } else {
                    //console.log(response);
                    sessionFactory.loginUser(response.data);
                    $state.go('profile');
                }
            }, function (err) {
                $scope.errormsg ="Server Error";
            })
        }
        $scope.login = function (user) {
            sessionFactory.login(user).then(function (response) {
                if (response.data.error) {
                    $scope.errormsg = response.data.error;
                } else {
                    sessionFactory.loginUser(response.data);
                    $state.go('profile');
                }
            }, function (err) {
                $scope.errormsg ="Server Error";
            })
        }



    }]);
} ());