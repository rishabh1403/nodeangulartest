(function () {
    angular.module('myApp').controller('profileCtrl', ['$scope', '$http', 'sessionFactory', '$state',
        function ($scope, $http, sessionFactory, $state) {
            $scope.msg = "";
            $scope.profileData = {
                "name": "",
                "email": "",
                "usertype": "",
                "profileImg": "",
                "addLineOne": "",
                "addLineTwo": "",
                "profileImg": "",
                "pincode": "",
                "status": "false"
            }
            $scope.getProfile = function () {
                var data = {
                    "token": sessionFactory.currentUser()
                }
                sessionFactory.getProfile(data).then(function (response) {
                    if (response.data.error) {
                    $scope.msg="Error Getting Your Profile";
                    } else {
                        $scope.profileData = response.data;
                    }
                }, function (err) {
                    $scope.msg="Error Saving Your Profile";
                })
            }
            $scope.getProfile();
            $scope.updateProfile = function (profileData) {
                var tempData = {
                    "token": sessionFactory.currentUser(),
                    "profile": $scope.profileData
                }
                sessionFactory.updateProfile(tempData).then(function (response) {
                    if (response.data.error) {
                        $scope.msg="Error Saving Your Profile";
                    } else {
                        $scope.msg="Saved Your Profile";
                    }
                }, function (err) {
                    $scope.msg="Error Saving Your Profile";
                })
            }
            $scope.logout = function () {
                sessionFactory.logout();
                $state.go('home');
            }

        }]);
} ());