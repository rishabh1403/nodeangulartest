(function () {

    angular.module('myApp').run(function ($rootScope, $state, sessionFactory) {
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {

                var isNavigatingTohome = toState.name === "home";
                //var isNavigatingToregister = toState.name === "register";
                //var isNavigatingTointro = toState.name === "intro";
                if (isNavigatingTohome) {

                    if (sessionFactory.isLoggedIn()) {
                        $state.go('profile');
                        event.preventDefault();

                    } else {
                        return;
                    }
                }
                if (!sessionFactory.isLoggedIn()) {
                    $state.go('home');
                    event.preventDefault();

                } else {
                    return;
                }


            });
    });
}());