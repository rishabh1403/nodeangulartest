(function () {
    angular.module('myApp').factory('sessionFactory',['configFactory','$http',function (configFactory,$http) {
        var user = "";
        var session = {
            isLoggedIn: function () {
                if (localStorage.getItem('token'))
                    return true;
                else
                    return false;
            },
            
            register: function (tempData) {
                return $http.post(configFactory.getRegisterUrl(), tempData)
            },
            getProfile: function (tempData) {
                return $http.post(configFactory.getProfileUrl(), tempData)
            },
            updateProfile: function (tempData) {
                return $http.post(configFactory.getProfileUpdateUrl(),tempData)
            },
            login: function (tempData) {
                return $http.post(configFactory.getLoginUrl(), tempData)
            },
            currentUser: function () {
                if (session.isLoggedIn)
                    return JSON.parse(localStorage.getItem('token'));

            },
            logout: function () {
                localStorage.clear();
            },
            loginUser: function (user) {
                localStorage.setItem('token', JSON.stringify(user));
            }

        };
        return session;
    }]);
}());