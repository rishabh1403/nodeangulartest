(function () {
    angular.module('myApp').factory('configFactory', function () {
        var baseUrl = "http://localhost:4000/api/";
        var config = {
            getBaseUrl: function () {
                return baseUrl;
            },
            getRegisterUrl:function(){
                return baseUrl+'user/register';
            },
            getLoginUrl:function(){
                return baseUrl+'user/login';
            },
            getProfileUrl:function(){
                return baseUrl+'profile/get';
            },
            getProfileUpdateUrl:function(){
                return baseUrl+'profile/update';
            }


        };
        return config;
    });
}());